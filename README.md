# Python 練習紀錄

*環境安裝過程中可能需要一些基本的docker知識。*

## 建構Python執行環境

1. 建構docker映像

指令：`docker compose build`

2. 啟動docker容器

指令：`docker compose up -d`

## 進入Python執行環境

1. 進入docker容器

指令：`docker compose exec -it python_container bash`

## 練習

1. 猜數字小遊戲

猜出由 1 ~ 100 隨機抽選的正確數字。

指令：`python scripts/BullsAndCows.py`

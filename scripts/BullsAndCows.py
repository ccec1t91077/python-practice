from random import randint

answer, guess = randint(1, 100), 0
min, max = 1, 100

while answer != guess:
    try:
        guess = int(input('\n請在 ' + str(min) + ' 到 ' + str(max) + ' 之間猜一個數:'))
    except:
        print('請輸入正常的數字，不要加其他字母或符號呦！')
        continue
    if guess < min or guess > max:
        print('請輸入正確範圍內的數字！')
        continue
    if guess < answer:
        print('您猜的數字比答案還要小，請再猜大一點～')
        min = guess + 1
    elif guess > answer:
        print('您猜的數字比答案還要大，請再猜小一點～')
        max = guess - 1

print('恭喜你猜出答案啦！')
